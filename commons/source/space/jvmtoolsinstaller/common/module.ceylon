native("jvm")
module space.jvmtoolsinstaller.common "1.0.0" {
	shared import ceylon.collection "1.3.2";
	shared import ceylon.time "1.3.3";
	shared import io.vertx.ceylon.web "3.4.2";
	import ceylon.json "1.3.2";
}
