import io.vertx.ceylon.core {
	Verticle
}
import io.vertx.ceylon.web {
	router,
	RoutingContext
}
import io.vertx.ceylon.web.handler {
	bodyHandler
}
import ceylon.json {
	JsonArray,
	JsonObject
}

class RestApi(ServiceInterface service) extends Verticle(){
	"Starts the rest service with the appropiate endpoints"
	shared actual void start(){
		value routerTemp = router.router(vertx);
		routerTemp.route().handler(bodyHandler.create().handle);
		routerTemp.get("/binarys").handler(handleGetAllBinarys);
		routerTemp.get("/binary/:version").handler(handleGetBinarySince);
		vertx.createHttpServer().requestHandler(routerTemp.accept).listen(8080);
		
	}
	"Get all Binarys of this endpoint"
	void handleGetAllBinarys(RoutingContext routingContext){
		value allBinarys = this.service.getAllBinarys();
		value response = routingContext.response();
		response.putHeader("content-type", "application/json").end(allBinarys.getJson());
	}
	void handleGetBinarySince(RoutingContext routingContext){
		value versionSince = routingContext.request().getParam(":version");
		value response = routingContext.response();
		if(exists versionSince){
			value binarys = {for(binary in this.service.getNewestSince(versionSince)) binary.getJson()};
			value jsonobject = JsonObject{
				"latest"->this.service.getLatestersion().getJsonString(),
				"binarys"-> JsonArray(binarys).string
			};
			response.putHeader("content-type", "application/json").end(jsonobject.string);
		}
	}
	
}