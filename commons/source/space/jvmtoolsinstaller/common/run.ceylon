"Run the module `space.jvmtoolsinstaller.common`."
shared void run() {
	{ServiceInterface*} services = `module`.findServiceProviders(`ServiceInterface`);
	assert(exists service = services.first);
	RestApi(service).start();
	
    
}