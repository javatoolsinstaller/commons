shared interface ServiceInterface {
	"Get all current [[Binary]]"
	shared formal BinaryRepo getAllBinarys();
	"Get all [[Binary]] since [[version]]"
	shared formal List<Binary> getNewestSince(String version);
	"Get the latest version of the binary"
	shared formal Binary getLatestersion();
}