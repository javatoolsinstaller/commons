import ceylon.json {
	JsonObject
}
shared class Binary(String group,String version,String url) {
	"Get object as Json String"
	shared String getJsonString(){
		
		return getJson().string;
	}
	
	shared JsonObject getJson(){
		value json = JsonObject{
			"group"-> this.group,
			"version"-> this.version,
			"url"-> this.url
		};
		return json;
	}
	
	
	
	
}