"Contains the server. Needed to start the server"
shared class WebEndServer(ServiceInterface serviceInterface) {
	"Start the server"
	shared void startServer(){
		RestApi(serviceInterface).start();
	}
	
	
}