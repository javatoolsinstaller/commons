import ceylon.collection {
	ArrayList
}
import ceylon.json {
	JsonObject,
	JsonArray
}
import java.util {
	StringJoiner
}


shared class BinaryRepo(Binary latestVersion) {
	shared ArrayList<Binary> binarys = ArrayList<Binary>();
	
	
	
	shared String getJson(){
		
		StringJoiner builder = StringJoiner(",");
		for(binary in binarys){
			builder.add(binary.getJsonString());
		}
		value json = JsonObject{
			"latest" -> this.latestVersion.getJsonString(),
			"binarys"-> JsonArray{
				builder.string
			}
		};
		return json.string;
	}
}